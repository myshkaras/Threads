#include "functions.h"

/* 
----- Algorithm -----
1 text file is parsed to chunks by one thread
2 chunk is sent to the queue
3 thread gets chunk from queue
4 thread cyphers chunk
5 thread puts cyphered chunk to the queue
6 another thread takes cyphered chunk and puts it to the resulting file

7 cyphered text is decyphered

Make configurator and a config struct that contains indices of objs in argv

----- TO DO -----
 - command line parser LATER
 - checker of files (input and output)
 - 
 */

unsigned long int currChunkId = 1;


//STAILQ_HEAD(stailhead, entry);

int main(int argc, char *argv[])
{
    encrypt(argv);

    return 0;
}

/* STAILQ_INIT(&head);                     

    fileChunk_t chunk1 = {.id=1, .body="123456789\0"};
    fileChunk_t chunk2 = {.id=2, .body="234567891\0"};
    fileChunk_t chunk3 = {.id=3, .body="345678912\0"};
    fileChunk_t chunk4 = {.id=4, .body="456789123\0"};
    fileChunk_t chunk5 = {.id=5, .body="567891234\0"};
    fileChunk_t chunk6 = {.id=6, .body="678912345\0"};
    fileChunk_t chunk7 = {.id=7, .body="789123456\0"};

    queuePush(&chunk6, &head);
    queuePush(&chunk1, &head);
    queuePush(&chunk2, &head);
    queuePush(&chunk5, &head);
    queuePush(&chunk3, &head);
    queuePush(&chunk7, &head);
    queuePush(&chunk4, &head);

    struct entry entry;
    entry = getNextEntryFromQueue(&head);
    printf("El#%lu: %s\n", entry.chunk.id, entry.chunk.body);
    ++currChunkId;
    
    entry = getNextEntryFromQueue(&head);
    printf("El#%lu: %s\n", entry.chunk.id, entry.chunk.body);
    ++currChunkId;
    
    entry = getNextEntryFromQueue(&head);
    printf("El#%lu: %s\n", entry.chunk.id, entry.chunk.body);
    ++currChunkId;
    
    entry = getNextEntryFromQueue(&head);
    printf("El#%lu: %s\n", entry.chunk.id, entry.chunk.body);
    ++currChunkId;

    entry = getNextEntryFromQueue(&head);
    printf("El#%lu: %s\n", entry.chunk.id, entry.chunk.body);
    ++currChunkId;

    entry = getNextEntryFromQueue(&head);
    printf("El#%lu: %s\n", entry.chunk.id, entry.chunk.body);
    ++currChunkId;

    entry = getNextEntryFromQueue(&head);
    printf("El#%lu: %s\n", entry.chunk.id, entry.chunk.body);
    ++currChunkId;
    
    if (!STAILQ_EMPTY(&head)) printf("The queue is not empty!!!\n");
    else printf("The queue is empty!\n");

    exit(EXIT_SUCCESS); */
