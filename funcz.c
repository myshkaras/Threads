#include "funcz.h"

extern unsigned long int currChunkId;

// ------------------------ Working with files ------------------------
void printCharArrI (char *arr, long unsigned int size)
{
    printf ("The buffer internals are:\n");
    for (long unsigned int i = 0; i < size; ++i)
    {
        printf("%i", arr[i]);
    }
    printf("\nSize: %lu, \nStrlen: %lu\n", size, strlen(arr));
}

void printCharArrC (char *arr, long unsigned int size)
{
    printf ("The buffer internals are:\n");
    for (long unsigned int i = 0; i < size; ++i)
    {
        printf("%c", arr[i]);
    }
    printf("\nSize: %lu, \nStrlen: %lu\n", size, strlen(arr));
}

void chunkInit (fileChunk_t *chunk, unsigned long int id)
{
    chunk->id = id;
    memset(chunk->body, '\0', sizeof(chunk->body));
}

int getChunk (FILE *filePointer, fileChunk_t *chunk)
{
    for (unsigned long int i = 0; i < CHUNK_SIZE; ++i)
    {
        char c = fgetc(filePointer);
        if (c == EOF) break;
        else chunk->body[i] = c;
    }

    return 0;
}

void writeChunk (FILE *filePointer, fileChunk_t *chunk)
{
    fputs(chunk->body, filePointer);
}

unsigned long int getFileLength (FILE *filePointer)
{
    fseek(filePointer, 0, SEEK_END);
    unsigned long int fileLen = ftell(filePointer);
    rewind(filePointer);

    return (fileLen + 1);
}

// ------------------------ Working with queues ------------------------
STAILQ_HEAD(stailhead, entry);

struct stailhead * queueInit (void)
{
    struct stailhead *head_p = malloc(sizeof(struct stailhead));
    STAILQ_INIT(head_p);

    return head_p;
}

int queuePush(fileChunk_t *chunk, struct stailhead *head_p)
{
    struct entry *newNode;
    newNode = malloc(sizeof(struct entry));
    newNode->chunk = *chunk;
    STAILQ_INSERT_TAIL(head_p, newNode, entries);

    return 0;
}

int queuePop (struct stailhead *head_p)
{
    if (STAILQ_EMPTY(head_p)) return 1;

    struct entry *firstElement = STAILQ_FIRST(head_p);
    STAILQ_REMOVE_HEAD(head_p, entries);
    //don't forget to free memory later in your script
    free(firstElement);

    return 0;
}

//may be useless
struct entry getNextEntryFromQueue (struct stailhead *head_p)
{
    struct entry *entry = STAILQ_FIRST(head_p);
    while (entry->chunk.id != currChunkId)
    {
        entry = STAILQ_NEXT(entry, entries);
    }
    struct entry resEntry = *entry;
    STAILQ_REMOVE(head_p, entry, entry, entries);
    return resEntry;
}

struct entry getEntryFromQueue (struct stailhead *head_p, unsigned long int chunkId)
{
    struct entry *entry = STAILQ_FIRST(head_p);
    while (entry->chunk.id != chunkId)
    {
        entry = STAILQ_NEXT(entry, entries);
    }
    struct entry resEntry = *entry;
    STAILQ_REMOVE(head_p, entry, entry, entries);
    return resEntry;
}

// ------------------------ Working with threads ------------------------
void* thread_square_func(void* arg) 
{
    int *number = (int *)arg;
    *number = (*number) * (*number);
    int sleep_length = *number%5;
    sleep(sleep_length);

    return (void *)number;
}

void* thread_func(void* arg) 
{
    int *number = (int *)arg;
    *number = (*number) * (*number);
    int sleep_length = *number%5;
    sleep(sleep_length);
    
    return (void *)number;
}

/* int main(void) 
{ 
    int arrOfValues[] = {1,2,3,4,5,6,7,8,9};
    int valArrLen = sizeof(arrOfValues)/sizeof(int);
    pthread_t arrofThreads[valArrLen];
    void* results[valArrLen];

    for (int i = 0; i < valArrLen; ++i)
    {
        pthread_create(&arrofThreads[i], NULL, thread_func, &arrOfValues[i]);
    }

    for (int i = 0; i < valArrLen; ++i)
    {
        pthread_join(arrofThreads[i], &results[i]);
        printf("Resulting value from thread_%i = %i\n",(i+1), *(int *)results[i]);
    }
 
    pthread_exit(NULL);
} */

// ------------------------ Preparations ------------------------
bool argsAmountOK (int argc)
{
    if (argc == 2) return true;
    return false;
}

char *checkFileName(int argc,char *argv[])
{
    if (!argsAmountOK(argc)) return "";

    printf ("File path: %s\n", argv[1]);

    return argv[1];

}
