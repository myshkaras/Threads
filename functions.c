#include "functions.h"

// ------------------------ The Main Function ------------------------
void encrypt (char *argv[])
{
    FILE *fp = fopen(argv[1], "r");
    if(fp == NULL) printf("Error opening file with data to encrypt\n");

    FILE *encfp = fopen ("encrypted_text.txt", "w");
    if(encfp == NULL) printf("Error opening file for encrypted data\n");

    FILE *decfp = fopen ("decrypted_text.txt", "w");
    if(decfp == NULL) printf("Error opening file for decrypted data\n");

    unsigned long int fileLen = getFileLength(fp);
    unsigned long int amountOfChunks = (fileLen / CHUNK_SIZE) + 1;

    struct stailhead *head1_p = queueInit();

    STAILQ_INIT(head1_p);

    for (unsigned long int i = 0; i < amountOfChunks; ++i)
    {
        fileChunk_t *chunk = (fileChunk_t*)malloc(sizeof(fileChunk_t));
        chunkInit(chunk, i);
        getChunk(fp, chunk);
        queuePush(chunk, head1_p);
    }

    for (unsigned long int i = 0; i < amountOfChunks; ++i)
    {
        struct entry* curr_entry = (struct entry*)malloc(sizeof(struct entry));
        if (queuePop(head1_p, curr_entry) == OK)
        {
            encryptChunk(&curr_entry->chunk);
            writeChunk(encfp, &curr_entry->chunk);
            decryptChunk(&curr_entry->chunk);
            writeChunk(decfp, &curr_entry->chunk);
            free(curr_entry);
        }
        else printf("There was come problem\n");
    }

    free(head1_p);

    if(fclose(fp)) printf("Error closing file with data to encrypt\n");
    if(fclose(encfp)) printf("Error closing file with encrypted data\n");
    if(fclose(decfp)) printf("Error closing file with decrypted data\n");
}

// ------------------------ Preparations ------------------------
/* bool argsAmountOK (int argc)
{
    if (argc == 2) return true;
    return false;
} */

/* bool checkFileName(int argc,char *argv[])
{
    if (!argsAmountOK(argc)) return false;

    printf ("File path: %s\n", argv[1]);

    return true;

} */

/* bool validateCommandLine (int argc,char *argv[])
{
    if (argc == 1) 
    {
        printf("No arguments passed\n");
        return false;
    }

    for (unsigned int i = 0; i < argc; ++i)
    {
        if ((argv[i][0] == '-') && (strlen(argv[i]) == 2)) continue;
        else return false;
    }
} */

/* struct config configInit (int argc,char *argv[])
{
    for (unsigned int i = 1; i < argc; ++i)
    {
        switch (argv[i])
        {
            case strcmp(argv[i][1], 'h'): break;

            default:
                break;

        }
    }
} */

// ------------------------ Working with files ------------------------
unsigned long int getFileLength (FILE *filePointer)
{
    fseek(filePointer, 0, SEEK_END);
    unsigned long int fileLen = ftell(filePointer);
    rewind(filePointer);

    return (fileLen + 1);
}

void chunkInit (fileChunk_t *chunk, unsigned long int id)
{
    chunk->id = id;
    memset(chunk->body, '\0', sizeof(chunk->body));
}

int getChunk (FILE *filePointer, fileChunk_t *chunk)
{
    for (unsigned long int i = 0; i < CHUNK_SIZE; ++i)
    {
        char c = fgetc(filePointer);
        if (c == EOF) break;
        else chunk->body[i] = c;
    }

    return 0;
}

void writeChunk (FILE *filePointer, fileChunk_t *chunk)
{
    fputs(chunk->body, filePointer);
}

// ------------------------ Threads Funcs ------------------------
/* void* encryptChunk (struct entry *entry)
{
    for (int i = 0; i < (CHUNK_SIZE + 1); ++i)
    {
        int symbol = (int)entry->chunk.body[i];
        symbol = (symbol * 2) + 3;
        entry->chunk.body[i] = (char) symbol;
    }
} */

void encryptChunk (fileChunk_t *chunk)
{
    for (int i = 0; i < (CHUNK_SIZE + 1); ++i)
    {
        unsigned int symbol = (int) (chunk->body[i]);
        symbol = (symbol + 3) % 254;
        chunk->body[i] = (char) (symbol);
    }
}

void decryptChunk (fileChunk_t *chunk)
{
    for (int i = 0; i < (CHUNK_SIZE + 1); ++i)
    {
        unsigned int symbol = (int) (chunk->body[i]);
        symbol = symbol - 3;
        chunk->body[i] = (char) (symbol);
    }
}

// ------------------------ Queue Funcs ------------------------
//STAILQ_HEAD(stailhead, entry);

struct stailhead *queueInit (void)
{
    struct stailhead *head_p = malloc(sizeof(struct stailhead));
    STAILQ_INIT(head_p);

    return head_p;
}

int queuePush(fileChunk_t *chunk, struct stailhead *head_p)
{
    struct entry *newNode = malloc(sizeof(struct entry));
    newNode->chunk = *chunk;
    free(chunk);
    STAILQ_INSERT_TAIL(head_p, newNode, entries);

    return 0;
}

enum Encr_error queuePop (struct stailhead* head_p, struct entry* entry_return)
{
    if (STAILQ_EMPTY(head_p)) return STAILQ_IS_EMPTY;

    struct entry *firstElement = STAILQ_FIRST(head_p);
    STAILQ_REMOVE_HEAD(head_p, entries);
    *entry_return = *firstElement;

    free(firstElement);

    return OK;
}

/* fileChunk_t* queuePop (struct stailhead *head_p)
{
    if (STAILQ_EMPTY(head_p)) return 1;

    struct entry *firstElement = STAILQ_FIRST(head_p);
    STAILQ_REMOVE_HEAD(head_p, entries);
    fileChunk_t *chunk = (fileChunk_t*)malloc(sizeof(fileChunk_t));
    *chunk = firstElement->chunk;
    //don't forget to free memory later in your script
    free(firstElement);

    return chunk;
} */

//may be useless
/* struct entry getNextEntryFromQueue (struct stailhead *head_p)
{
    struct entry *entry = STAILQ_FIRST(head_p);
    while (entry->chunk.id != currChunkId)
    {
        entry = STAILQ_NEXT(entry, entries);
    }
    struct entry resEntry = *entry;
    STAILQ_REMOVE(head_p, entry, entry, entries);
    return resEntry;
} */

struct entry getEntryFromQueue (struct stailhead *head_p, unsigned long int chunkId)
{
    struct entry *entry = STAILQ_FIRST(head_p);
    while (entry->chunk.id != chunkId)
    {
        entry = STAILQ_NEXT(entry, entries);
    }
    struct entry resEntry = *entry;
    STAILQ_REMOVE(head_p, entry, entry, entries);
    return resEntry;
}
