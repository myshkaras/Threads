#pragma once
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/queue.h>
#include <stddef.h>

#define CHUNK_SIZE 10

enum Encr_error
{
    OK = 0,
    STAILQ_IS_EMPTY = 1,
};

struct config 
{
    unsigned int filePath;
    unsigned int workersAmount;
    unsigned int outputFilePath;
    unsigned int encDecMode;
};

/* #include <stdlib.h>
#include <pthread.h>
#include <unistd.h> //for sleep() */

typedef struct
{
    unsigned long int id;
    char body[CHUNK_SIZE + 1]; // +1 is for \0 symbol at the end of array 

} fileChunk_t;

struct entry 
{
    fileChunk_t chunk;
    STAILQ_ENTRY(entry) entries;
};

// ------------------------ The Main Function ------------------------
void encrypt (char *argv[]);


// ------------------------ Preparations ------------------------
bool argsAmountOK (int argc);

bool checkFileName(int argc,char *argv[]);

struct config configInit (int argc,char *argv[]);

// ------------------------ Working with files ------------------------
unsigned long int getFileLength (FILE *filePointer);

void chunkInit (fileChunk_t *chunk, unsigned long int id);

int getChunk (FILE *filePointer, fileChunk_t *chunk);

void writeChunk (FILE *filePointer, fileChunk_t *chunk);

// ------------------------ Threads Funcs ------------------------
/* void* encryptChunk (struct entry *entry); */

void encryptChunk (fileChunk_t *chunk);

void decryptChunk (fileChunk_t *chunk);


// ------------------------ Queue Funcs ------------------------
STAILQ_HEAD(stailhead, entry);

struct stailhead * queueInit (void);

int queuePush(fileChunk_t *chunk, struct stailhead *head_p);

enum Encr_error queuePop (struct stailhead* head_p, struct entry* entry_return);

//may be useless
/* struct entry getNextEntryFromQueue (struct stailhead *head_p); */

struct entry getEntryFromQueue (struct stailhead *head_p, unsigned long int chunkId);
