#pragma once
#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <sys/queue.h>
#include <pthread.h>
#include <unistd.h> //for sleep()


#define CHUNK_SIZE 10

// ------------------------ Working with files ------------------------
typedef struct
{
    unsigned long int id;
    char body[CHUNK_SIZE + 1]; // +1 is for \0 symbol at the end of array 

} fileChunk_t;

void printCharArrI (char *arr, long unsigned int size);

void printCharArrC (char *arr, long unsigned int size);

void chunkInit (fileChunk_t *chunk, unsigned long int id);

int getChunk (FILE *filePointer, fileChunk_t *chunk);

void writeChunk (FILE *filePointer, fileChunk_t *chunk);

unsigned long int getFileLength (FILE *filePointer);

// ------------------------ Working with queues ------------------------
struct entry {
    fileChunk_t chunk;
    STAILQ_ENTRY(entry) entries;
};

struct stailhead * queueInit (void);

int queuePush(fileChunk_t *chunk, struct stailhead *head_p);

int queuePop (struct stailhead *head_p);

//may be useless
struct entry getNextEntryFromQueue (struct stailhead *head_p);

struct entry getEntryFromQueue (struct stailhead *head_p, unsigned long int chunkId);

// ------------------------ Working with queues ------------------------
void* thread_square_func(void* arg);

void* thread_func(void* arg);


/* // ------------------------ Preparations ------------------------
bool argsAmountOK (int argc);

char *checkFileName(int argc,char *argv[]); */
