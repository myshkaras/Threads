CFLAGS := -pedantic -Wall
CFLAGS_DEB := -pedantic -Wall -g
COMPILER := clang

all: del thr work

debug: del thr_deb work_deb

mem-check: 
	valgrind --track-origins=yes -s ./threads text.txt
	
thr:	
	@$(COMPILER) main.c functions.c -o threads $(CFLAGS)
	$(info Compiling...)

thr_deb:
	@$(COMPILER) main.c functions.c -o threads_debug $(CFLAGS_DEB)
	$(info Compiling for debug...)

work: 
	@./threads text.txt
	$(info Executing...)

work_deb:
	@gdb ./threads_debug text.txt
	$(info Executing debug...)

del:
	@rm -f encrypted_text.txt && rm -f decrypted_text.txt
	$(info Deleting encrypted and decrypted text...)

clean:
	rm -f threads